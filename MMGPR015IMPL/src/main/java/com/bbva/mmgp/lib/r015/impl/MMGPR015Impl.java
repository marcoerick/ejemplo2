package com.bbva.mmgp.lib.r015.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.apx.exception.db.DuplicateKeyException;
import com.bbva.mmgp.lib.r015.MMGPR015;

public class MMGPR015Impl extends MMGPR015Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(MMGPR015.class);
	
	@Override
	public int executeInsert(Map<String,Object> transference){
		LOGGER.info("Inicio del insert en la tabla");
		try{
			this.jdbcUtils.update("sql.insertTransference", transference);
			LOGGER.info(transference.toString());
			return 1;
		}catch(DuplicateKeyException dke){
			LOGGER.info("Error al insertar transferencia",dke);
			return 0;
		}
	}
}
