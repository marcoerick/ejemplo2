package com.bbva.mmgp.lib.r015.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.library.AbstractLibrary;
//import com.bbva.elara.utility.jdbc.connector.JdbcUtils;
import com.bbva.elara.utility.jdbc.JdbcUtils;
import com.bbva.mmgp.lib.r015.MMGPR015;

public abstract class MMGPR015Abstract extends AbstractLibrary implements MMGPR015 {

	private static final Logger LOGGER = LoggerFactory.getLogger(MMGPR015.class);
	
	protected ApplicationConfigurationService applicationConfigurationService;
	
	protected JdbcUtils jdbcUtils;
	
	/**
	 * @param applicationConfigurationService the applicationConfigurationService to set
	 */
	public void setApplicationConfigurationService(
			ApplicationConfigurationService applicationConfigurationService) {
		this.applicationConfigurationService = applicationConfigurationService;
	}
	
	
	/**
	 * @param jdbcUtils the jdbcUtils to set
	 */
	public void setJdbcUtils(JdbcUtils jdbcUtils) {
		this.jdbcUtils = jdbcUtils;
	}
}
