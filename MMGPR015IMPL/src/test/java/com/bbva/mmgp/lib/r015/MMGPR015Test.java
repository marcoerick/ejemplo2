package com.bbva.mmgp.lib.r015;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import com.bbva.elara.utility.jdbc.JdbcUtils;
import com.bbva.mmgp.lib.r015.impl.MMGPR015Impl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/MMGPR015-app.xml",
		"classpath:/META-INF/spring/MMGPR015-app-test.xml",
		"classpath:/META-INF/spring/MMGPR015-arc.xml",
		"classpath:/META-INF/spring/MMGPR015-arc-test.xml" })
public class MMGPR015Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(MMGPR015.class);
	
	@InjectMocks
    private MMGPR015Impl mmgpR015;
    
    @Spy
    private Context context;
    
    @Mock
    private JdbcUtils jdbcUtils;
	
/*	@Resource(name = "mmgpR015")
	private MMGPR015 mmgpR015;*/

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		ThreadContext.set(context);
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.mmgpR015;
		if(this.mmgpR015 instanceof Advised){
			Advised advised = (Advised) this.mmgpR015;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}


	@Test
	public void executeInsertTest(){
		LOGGER.info("Executing the test...");
		Map<String,Object> params=new HashMap<>();
		Mockito.when(jdbcUtils.update(anyString(),anyMap())).thenReturn(anyInt());
		mmgpR015.executeInsert(params);
	}
		
}
